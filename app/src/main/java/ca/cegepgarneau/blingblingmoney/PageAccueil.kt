package ca.cegepgarneau.blingblingmoney

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class PageAccueil : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accueil)

        val nomUser = intent.getStringExtra("USERNAME_TEXT")
        findViewById<TextView>(R.id.txtWelcome).text = "$nomUser"

        val sharedPreference = getSharedPreferences("BlingBlingMoney", Context.MODE_PRIVATE)
        val argentDisponible = sharedPreference.getInt(nomUser, -1)
        findViewById<TextView>(R.id.tvSolde).text = "Votre solde : $argentDisponible"

        val btnJouer = findViewById<Button>(R.id.btnJouer)
        btnJouer.setOnClickListener {
            // Validation que l'utilisateur ait de l'argent
            if (argentDisponible == 0){
                // Lance l'activité de la Banque
                intent = Intent(this, Banque::class.java)
                intent.putExtra("USERNAME_TEXT", nomUser)
                startActivity(intent)
            }

            // Lance l'activité Roulette
            intent = Intent(this, Roulette::class.java)
            intent.putExtra("USERNAME_TEXT", nomUser)
            startActivity(intent)
        }

        // Lance l'activité de la Banque
        val btnBanque = findViewById<Button>(R.id.btnBanque)
        btnBanque.setOnClickListener {
            intent = Intent(this, Banque::class.java)
            intent.putExtra("USERNAME_TEXT", nomUser)
            startActivity(intent)
        }



    }

}