package ca.cegepgarneau.blingblingmoney

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Banque : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banque)
        val tvMontant = findViewById<TextView>(R.id.textMontant)
        tvMontant.transformationMethod = null

        // Obtiens le nom du user passé dans l'intent
        val nomUser = intent.getStringExtra("USERNAME_TEXT")

        val btnConfirmer = findViewById<Button>(R.id.btnConfirmer)
        btnConfirmer.setOnClickListener {
            // Montant souhaité
            val montant = Integer.parseInt(tvMontant.text.toString())

            // Valide le montant
            if (montant >= 0){
                // Connexion au sharedPreference
                val sharedPreference = getSharedPreferences("BlingBlingMoney", Context.MODE_PRIVATE)
                // Ajoute le montant au montant actuel
                val newMontant = montant + sharedPreference.getInt(nomUser,0)
                // Modifie les info dans le sharedPreference
                with(sharedPreference.edit()){
                    putInt(nomUser, newMontant)
                    apply()
                }
                // Affiche la page d'accueil
                intent = Intent(this, PageAccueil::class.java)
                intent.putExtra("USERNAME_TEXT", nomUser)
                startActivity(intent)
            }
            else{
                Toast.makeText(this, "Vous devez entrer un montant.", Toast.LENGTH_LONG).show()
            }
        }



    }
}