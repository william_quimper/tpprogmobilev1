package ca.cegepgarneau.blingblingmoney

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Connexion : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val btnConnexion = findViewById<Button>(R.id.btnConnexion)
        val txtUsername = findViewById<TextView>(R.id.editTextNom)

        // listener sur le bouton connexion
        btnConnexion.setOnClickListener {
            val mNomUtilisateur = txtUsername.text.toString()
            if (mNomUtilisateur != ""){
                val sharedPreference = getSharedPreferences("BlingBlingMoney", Context.MODE_PRIVATE)

                // Ajoute un nouvel utilisateur dans le SharedPreferences
                // Si celui-ci n'existe pas déjà
                if(sharedPreference.getInt(mNomUtilisateur, -1) == -1){
                    with (sharedPreference.edit()) {
                        putInt(mNomUtilisateur, 15)
                        apply()
                    }
                }
                // Lance le jeu avec le nom du joueur en parametre
                intent = Intent(this, PageAccueil::class.java)
                intent.putExtra("USERNAME_TEXT", mNomUtilisateur)
                startActivity(intent)
            }
            else{
                // Alerte à l'utilisateur
                Toast.makeText(this, "Veuillez entrer un nom", Toast.LENGTH_SHORT).show()
            }
        }
    }
}