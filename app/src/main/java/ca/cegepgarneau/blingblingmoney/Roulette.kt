package ca.cegepgarneau.blingblingmoney

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class Roulette : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roulette)
        val username = intent.getStringExtra("USERNAME_TEXT")
        val sharedPreference = getSharedPreferences("BlingBlingMoney", Context.MODE_PRIVATE)
        var argentRestant = sharedPreference.getInt(username,0)

        val radioBtnRouge : RadioButton = findViewById(R.id.miseRed)
        val radioBtnNoir : RadioButton = findViewById(R.id.miseBlack)
        var couleurCochee: String

        val numeroText = findViewById<EditText>(R.id.editTextMiseNumero)

        /**
         * Décoche le bouton radio noir lorsque
         * le rouge est activé
         */
        radioBtnRouge.setOnClickListener {
            if (radioBtnNoir.isChecked){
                radioBtnNoir.isChecked = false
            }
            numeroText.text.clear()
        }

        /**
         * Décoche le bouton radio rouge lorsque
         * le noir est activé
         */
        radioBtnNoir.setOnClickListener {
            if (radioBtnRouge.isChecked){
                radioBtnRouge.isChecked = false
            }
            numeroText.text.clear()
        }


        // Affiche le solde restant de l'utilisateur
        val miseText = findViewById<EditText>(R.id.editTextMise)
        findViewById<TextView>(R.id.tvSoldeRestant).text = "SOLDE : $argentRestant"

        // Action sur le bouton SPIN
        findViewById<Button>(R.id.spinBtn).setOnClickListener {
            // Obtiens le solde restant de l'utilisateur
            argentRestant = sharedPreference.getInt(username,0)

            val mise: Int
            var numero : Int = -1

            // Valide si l'utilisateur a entré une mise
            if(miseText.text.toString() == "" ){
                Toast.makeText(this,"Vous devez remplir les champs", Toast.LENGTH_SHORT).show()
            }
            else {
                mise = Integer.parseInt(miseText.text.toString())

                // Validation de la mise
                if (mise <= 0) {
                    Toast.makeText(this, "Vous devez entrer une mise valide", Toast.LENGTH_SHORT).show()
                }
                else{
                    // Enregistre la couleur cochée
                    couleurCochee = when {
                        radioBtnRouge.isChecked -> "rouge"
                        radioBtnNoir.isChecked -> "noir"
                        else -> "aucun"
                    }
                    
                    
                    // Vérifie si une des couleur a été cochée
                    if (couleurCochee != "aucun"){
                        // Préviens que l'utilisateur parie plus que la somme restante dans la banque
                        if(mise > argentRestant){
                            intent = Intent(this,Banque::class.java)
                            intent.putExtra("USERNAME_TEXT", username)
                            startActivity(intent)
                        }
                        else {
                            // Joue à la roulette avec la mise et la couleur pariée
                            val nouveauSolde = spinCouleurResultat(couleurCochee, mise, argentRestant)

                            // Modifie le solde restant dans le sharedPreference
                            with(sharedPreference.edit()) {
                                putInt(username, nouveauSolde)
                                apply()
                            }
                            // Ajuste le solde affiché
                            findViewById<TextView>(R.id.tvSoldeRestant).text = "SOLDE : $nouveauSolde"

                            // Affiche la banque si l'utilisateur n'a plus d'argent
                            if (nouveauSolde == 0){
                                intent = Intent(this,Banque::class.java)
                                intent.putExtra("USERNAME_TEXT", username)
                                startActivity(intent)
                            }

                        }
                    }
                    // Si aucune couleur n'a été cochée, le pari est sur un nombre précis
                    else{
                        // Vérifie le champs text
                        if(numeroText.text.toString() == "") {
                            numero = Integer.parseInt(numeroText.text.toString())
                        }

                        // Vérifie si le numéro parié est valide
                        if (numero <= 0 || numero > 36) {
                            Toast.makeText(
                                this,
                                "Le chiffre choisi doit se situer entre 0 et 36",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else {
                            // Vérifie si la mise n'est pas supérieure à l'argent restant
                            if(mise > argentRestant){
                                intent = Intent(this,Banque::class.java)
                                intent.putExtra("USERNAME_TEXT", username)
                                startActivity(intent)
                            } else {
                                // Jouer la mise sur le nombre
                                val nouveauSolde = spinMiseNumeroResultat(numero, mise, argentRestant)

                                // Ajuste le nouveau solde dans le sharedPreferences
                                with(sharedPreference.edit()) {
                                    putInt(username, nouveauSolde)
                                    apply()
                                }
                                findViewById<TextView>(R.id.tvSoldeRestant).text = "SOLDE : $nouveauSolde"

                                // Affiche la banque si l'utilisateur n'a plus d'argent
                                if (nouveauSolde == 0) {
                                    intent = Intent(this, Banque::class.java)
                                    intent.putExtra("USERNAME_TEXT", username)
                                    startActivity(intent)
                                }
                            }
                        }
                    }
                }
            }
            // Efface les champs text et décoche les boutons radio
            resetInformation()
        }
    }

    /**
     * couleur: couleur parié
     * mise: jeton(s) parié
     * argentRestant: solde de l'utilisateur avant le bet
     *
     */
    private fun spinCouleurResultat(couleur : String, mise : Int, argentRestant : Int): Int {
        val numberRouges: List<Int> = listOf(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36)
        val numberNoirs: List<Int> = listOf(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35)

        // Génère un nombre aléatoire
        val rnds = (0..36).random()

        // Vérifie si la mise est gagnante
        // et retroune le nouveau sole
        return if((couleur == "rouge" && numberRouges.contains(rnds)) ||
            (couleur == "noir" && numberNoirs.contains(rnds)))
            mise + argentRestant
        else
            argentRestant - mise

        afficherResultat(rnds)
    }

    /**
     * numero : numéro parié
     * mise: jetons parié(s)
     * argentRestant: solde de l'utilisateur avant le bet
     * return montantRetour: Solde de l'utilisateur après son bet
     *
     * Cette fonction génère un nombre aléatoire et retourne le nouveau solde
     * de l'utilisateur
     */
    private fun spinMiseNumeroResultat(numero : Int, mise : Int, argentRestant : Int): Int {
        var montantRetour: Int

        // Génère un nombre aléatoire
        val rnds = (0..36).random()

        // Vérifie si le bet est gagnant
        if(rnds == numero)
            montantRetour = mise * 35 + argentRestant
        else
            montantRetour = argentRestant - mise

        afficherResultat(rnds)

        return montantRetour
    }

    /**
     * numero: numeré aléatoire généré
     * Cette fonction modifie l'affichage selon le nombre aléatoirement
     * généré.
     */
    private fun afficherResultat(numero:Int){
        val resulat = findViewById<TextView>(R.id.betResult)

        // Les nombres pairs sont rouge et les nombres impairs sont noir
        val numberRouges: List<Int> = listOf(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36)
        val numberNoirs: List<Int> = listOf(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35)

        // Modifie la couleur du numero affiché
        if (numberRouges.contains(numero))
            resulat.setTextColor(Color.RED)
        else if(numberNoirs.contains(numero))
            resulat.setTextColor(Color.BLACK)
        else
            resulat.setTextColor(Color.GREEN)

        // Affiche le nombre généré
        resulat.text = numero.toString()
    }

    private fun resetInformation(){
        val radioButtonRed : RadioButton = findViewById(R.id.miseRed)
        val radioButtonBlack : RadioButton = findViewById(R.id.miseBlack)
        radioButtonRed.isChecked = false
        radioButtonBlack.isChecked = false


    }
}